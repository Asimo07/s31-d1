// importing the model
const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based on the Mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client/Postman
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	})
}

// Controller function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

// Controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr)
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((specificTask, er) => {
		if(er){
			console.log(error);
			return false;
		}
		else{
		return specificTask;
		}
	})	
}
	
module.exports.completeTask = (taskId, newInput) => {
	return Task.findById(taskId).then((result1, newError) => {
		if(newError){
			console.log(newError);
			return false;
		}

		result1.status = newInput.status;

		return result1.save().then((completeTask, newErr) => {
			if (newErr){
				console.log(newErr)
				return false;
			}
			else{
				return completeTask;
			}
		})
	})
}

